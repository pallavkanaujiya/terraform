module "my-module-vpc" {
source="../modules/vpc"
my-vpc=module.my-module-vpc.vpc_id
vpc_cidr_block="10.0.0.0/16"
subnet_cidr_block="10.0.1.0/24"
}

module "my-ec2" {
source="../modules/ec2"
subnet_id=module.my-module-vpc.subnet_id
}

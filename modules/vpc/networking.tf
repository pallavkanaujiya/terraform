resource "aws_vpc" "my-vpc" {
  cidr_block = var.vpc_cidr_block
  instance_tenancy="default"

 tags = {
  Name="MY-VPC"
 }
}

resource aws_subnet "MY-VPC-SUBNET" {
vpc_id=var.my-vpc
cidr_block=var.subnet_cidr_block

tags = {
Name="pallav-subnet"
}
}

output "vpc_id" {
value=aws_vpc.my-vpc.id
}
